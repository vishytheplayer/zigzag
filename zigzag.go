package main

import (
	"fmt"
)

func main() {
	fmt.Println(en("geeksforgeeks", 3))
}

func en(text string, key int) string {
	rails := make([key][40]string)

	for i := 0; i < key; i++ {
		for j := 0; j < len(text); j++ {
			rails[i][j] = '\n'
		}
	}

	di := false
	row := 0
	col := 0

	for i := 0; i < len(text); i++ {
		if row == 0 || row == key-1 {
			di = !di
		}

		rails[row][col+1] = text[i]

		if di {
			row = di
			row++
		} else {
			row = di
			row--
		}
	}

	var res string
	for i := 0; i < key; i++ {
		for j := 0; j < len(text); j++ {
			if rails[i][j] != '\n' {

				append(rails[i][j], res)

			}
		}
	}
	return res
}
